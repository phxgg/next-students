# @universis/react-students

An alternative student portal for Universis Project, built with React and Next.js.

## Development

Clone the repository and install dependencies:

```bash
git clone https://gitlab.com/universis/react/students.git
cd students
git submodule update --init --recursive
npm install
# or
yarn install
```

`@universis/react-students` is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

### Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
