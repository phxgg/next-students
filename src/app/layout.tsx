import type { Metadata } from "next";

export const metadata: Metadata = {
  title: "Universis Project | Student Portal",
  description: "An alternative student portal for Universis Project, built with React and Next.js.",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body>{children}</body>
    </html>
  );
}
